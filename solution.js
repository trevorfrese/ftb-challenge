
/*--------------TREVOR FRESE--------------
INTERVIEW QUESTION - TO TEST IF I'M GOOD ENOUGH FOR FINDTHEBEST
CHALLENGE TO FIND THE BEST INTERVIEWEE 
Find the treasure in the best way?
Find the worst headache? 

Algorithm description: I decided to tap into the infinite knowledge that is 
the internet and googled around for a bit on how to solve a non-simply connected graph 
with unknown start or destination, finite information about cells, and forced iteration.
Honestly the hardest part of all of those constraints is the forced iteration. 

FIND THE TREASURE
The algorithms that I found were all very recursive since the process itself is VERY recursive.
Maybe it's because I really like recursion and enjoy using it over iterative procedures but I went 
with the algorithm that incorporated backtracking as a way of "dead-end filling" the maze as you go along.

For a cool dead end filling demonstration check out this video: https://www.youtube.com/watch?v=yqZDYcpCGAI

Anywhoo I painstakingly created the non-recursive strategy to do backtracking which involved a stack and
involved checking the stack of coordinates I had traversed and making sense out of them.

BUILD THE STAIRCASE
After finding the treasure I built the staircase by first picking up a block as I traverse the maze and
keeping track of a 'basestep' which is the current bottom step of the staircase (of height 1).
So basically I create a staircase which always looks like this:

			 [ ][1][2]
			 [ ][T][3]     where T is the treasure, and the cells 1 -> 6
			 [6][5][4]     represent the order in which the steps are added

So the first basestep is at the 1 location, the 2nd at 2, etc.

I log all the blocks I've seen so far in the maze in a 'blocks' array, which gets searched for the closest block
to our current position (usually at the staircase).

After the troll runs out of blocks it uses the same traversal pattern from the "FIND THE TREASURE" portion and
now it walks until it finds a block, then brings it back. I also have a 'badpaths' list which contains bad paths 
that are long to traverse, and the troll avoids those paths in the future. I also used Ivan's advice in 
getting the troll to find all the blocks first, but sometimes this leads to issues still, in that my function
to walk to the blocks from a certain set of coordinates is not well defined enough to get to any block in space,
and also it determines after a number of steps if it is worth it to go to that block, or to just look for a new
block.

So then the guy walks up the staircase after he has stacked 6 steps instead of 7, he just places the 7th block
on the block of height 6 and then walks right up to the treasure and picks it up.

CONCLUSION
I made my own algorithm for this solution and I'm fairly satisfied with it. It is capable of getting down to below
600 in some cases (lowest I got was 514) and on average it is between 500-700, but the standard deviation was
fairly high in that it could go as low as 500 or as high as 1000. It was definitely cases in which the treasure
was in a enclosure (which would be hard for the troll to find the entrance and exit) or when the treasure 
was far from many blocks. 

The solution could be improved, but would require a large revamping. I thought of this idea two days ago:
mapping the entire cave by storing what every single coordinate is: wall, block, empty or treasure so that
the troll could never get lost or stuck. Unfortunately I didn't really think of that in time, so that solution would 
require modifying almost all of the code. 

The new solution works consistently and I'd say overall is pretty optimal. Thanks for reading and giving 
me the opportunity to interview! I really hope the code is satisfactory because I would love to work at FTB!


-----------------------------------------*/
function Stacker(){

var
EMPTY = 0,
WALL = 1,
BLOCK = 2,
GOLD = 3;

/*--------Global Var decs --------*/
var foundTreasure = false;
var treasureHeight = 0;
var holdingBlock = false;
var coords = [0,0]; //list of coords so far
var blocks = []; //list of blocks so far
var tcoords = [0,0]; //treasure coordinates
var traversed = [];
var backtracking = false;


var path = []; //path from staircase
var basestep = [0,0]; //first step of staircase
var dropnow; //bool telling whether to drop or not
var goingup = false;  //going up stairs
var basenum = 1; //initial step number were on
var goingdown = false; //going down stairs
var badpaths = []; //paths that you dont want to explore twice
var blocksneeded = 22; //total number of blocks needed for treasure of 8

var winning = false; //we won!

var direction = "up"; //this isnt necessary, i'll remove later
traversed.push([0,0]);


this.turn = function turner (cell){
//first things first, check on each cell for blocks nearby, add to blocks.
//also decrement total number of blocks 
		blocksneeded -=checkblock(cell, coords.slice(), basestep, blocks);
		upcoord = coords.slice(), downcoord = coords.slice(), leftcoord = coords.slice(), rightcoord = coords.slice();
		upcoord[1] += 1; downcoord[1] -= 1; leftcoord[0] -= 1; rightcoord[0] += 1; 
		blocksneeded -=checkblock(cell.up, upcoord, basestep, blocks);
		blocksneeded -=checkblock(cell.down, downcoord, basestep, blocks);
		blocksneeded -=checkblock(cell.right, rightcoord, basestep, blocks);
		blocksneeded -=checkblock(cell.left, leftcoord, basestep, blocks);
		

	//if you've found the treasure and you have 22 blocks, start building a staircase
	if(foundTreasure == true && blocksneeded <= 0){
		/*-------STAIRCASE BUILDING CODE----------*/

		//looks like we won. WOOO
		if(winning){
			if(coordmatch(coords, tcoords)){ //right on the treasure
				return "pickup";
			}
			else{ 	//go down one, onto the treasure
				return movedown(cell, coords, path, direction);
			}
		}

		//found a new base for the staircase with an empty first step
		if(holdingBlock && atbase(coords, basestep) && cell.type == 0){
			holdingBlock = false;
			path = [];
			goingdown = true;
			return "drop";
		}

		//troll is holding a block and walking up the staircase
		if(holdingBlock && goingup){
			if(cell.level == 6){ //we won almost!
				winning = true;
				return "drop";
			}
			if(dropnow){ //drop the block onto the staircase, resets a LOT of variables
				if(basenum == cell.level + 1)
				{ //sets a new base number, i.e. 1 -> 2 
					basestepper(basestep, tcoords, basenum);
					basenum++;
				} 
				holdingBlock = false;
				goingdown = true; 
				path = [];
				goingup = false; 
				dropnow = false; 
				return "drop";
			}

			currheight = cell.level;

			//newdirection uses walkup which finds the next step to go up on the staircase
			newdirection = walkup(coords,tcoords);
			//keep walking up the staircase until the next step is the height of the staircase, then drop
			if(newdirection == "up"){
					if(cell.up.level == currheight){
						dropnow = true;
					}
					return moveup(cell, coords, traversed, direction); 
				}
			if(newdirection == "left"){
				if(cell.left.level == currheight){
					dropnow = true;
				}
				return moveleft(cell, coords, traversed, direction); 
			}
			if(newdirection == "right"){
				if(cell.right.level == currheight){
					dropnow = true;
				}
				return moveright(cell, coords, traversed, direction); 
			}
		}
		//were holding a block at the base and a staircase exists
		if(holdingBlock && atbase(coords,basestep) && cell.type == 2){
			
			if(basenum == 1){ //first iteration of staircase, dont walk up, just move to the right
				basestepper(basestep, tcoords, basenum);
				basenum++;
				return moveright(cell, coords, traversed, direction);
			}
			//go up the staircase since its already greater than one
			if(cell.level >= 1){ //go up staircase now

				if(dropnow){ //drop the block and reset variables
					if(basenum == cell.level + 1){
						basestepper(basestep, tcoords, basenum);
						basenum++;
					}
					holdingBlock = false;
					goingdown = true;
					path = []; 
					dropnow = false;
					return "drop";
				}

				goingup = true;
				currheight = cell.level;
				//newdirection uses walkup which finds the next step to go up on the staircase
				newdirection = walkup(coords,tcoords);
				//walk up stairs
				if(newdirection == "up"){
					if(cell.up.level == currheight){
						dropnow = true;
					}
					return moveup(cell, coords, traversed, direction); 
				}
				if(newdirection == "left"){
					if(cell.left.level == currheight){
						dropnow = true;
					}
					return moveleft(cell, coords, traversed, direction); 
				}
				if(newdirection == "right"){
					if(cell.right.level == currheight){
						dropnow = true;
					}
					return moveright(cell, coords, traversed, direction); 
				}

			}
		}
		//not holding a block , walking down the staircase
		if(!holdingBlock && goingdown){
			//use a as a check whether you can go down further or if you're off the staircase
			a = walkdown(coords, tcoords, cell, path, direction);
			if(a != undefined){
				return a;
			}
			else{
				goingdown = false;
			}
		}

		//the troll just picked up a block and isnt at the base of the staircase
		if(holdingBlock && !atbase(coords,basestep)){
			//walk back to the base

			//if path is longer than 1, retrace the path
			if (path.length > 1){

				//if the path is greater than 15 its too big, add it to badpaths
				if(path.length > 15){
				//try a new route next time
				coordpusher(badpaths, coords);
				}
				return retrace(path, coords, cell, direction);
			}
			else{
				//close enough to staircase to walk delicately back up
				path = [];
				backtracking = false;
				return gotobase(coords, basestep, cell, traversed, direction); 
			}
		}
		
		//if not holding a block and not at the base
		if(!holdingBlock){

			//finds closest block nearby
			nextblock = findclosest(coords, blocks, tcoords);
			if(nextblock[0] == 100 && nextblock[1] == 100) { //if set to some garbage value, find a new set of blocks
				
				// walk in any direction trying to find a block
				if(backtracking == true){ //same as traversal code
					//look for a new direction from the cells around it, and make sure its not on badpaths, 
					//aka find a short path to a new block
					var newdir = newDirection2(coords.slice(), path, badpaths, cell);
					if(newdir == "up" && ((cell.up.type == 0) || (cell.up.type == 2 && cell.up.level <= 1)) )
						{backtracking = false;
						return moveup(cell, coords, path, direction);}
					if(newdir == "down" && ((cell.down.type == 0) || (cell.down.type == 2 && cell.down.level <= 1)) ) 
						{backtracking = false;
						
						return movedown(cell, coords, path, direction);}
					if(newdir == "left" && ((cell.left.type == 0) || (cell.left.type == 2 && cell.left.level <= 1)) ) 
						{backtracking = false;
						
						return moveleft(cell, coords, path, direction);}
					if(newdir == "right"&& ((cell.right.type == 0) || (cell.right.type == 2 && cell.right.level <= 1)) ) 
						{backtracking = false;
						
						return moveright(cell, coords, path, direction);}
					
					if(newdir == undefined){
						//couldnt find a new spot from any of these cells, go back one more on the path.
						tempdir = direction;
						direction = backtrack(path,coords.slice(), tempdir);
						if(direction == "up" && ((cell.up.type == 0) || (cell.up.type == 2 && cell.up.level <= 1)) )
							{
								return moveup(cell, coords, path, direction);}
						if(direction == "down" &&  ((cell.down.type == 0) || (cell.down.type == 2 && cell.down.level <= 1)) ) 
							{ 
								return movedown(cell, coords, path, direction);}
						if(direction == "left" && ((cell.left.type == 0) || (cell.left.type == 2 && cell.left.level <= 1)) ) 
							{
								return moveleft(cell, coords, path, direction);}
						if(direction == "right"&& ((cell.right.type == 0) || (cell.right.type == 2 && cell.right.level <= 1)) ) 
							{
								return moveright(cell, coords, path, direction);}

					}
				}
				tempdir = direction;	
				//find a new direction that doesnt go on badpaths
				var newdir = newDirection2(coords.slice(), path, badpaths, cell);
				if(newdir == "up" && ((cell.up.type == 0) || (cell.up.type == 2 && cell.up.level <= 1)) )
					{return moveup(cell, coords, path, direction);}
				if(newdir == "down" && ((cell.down.type == 0) || (cell.down.type == 2 && cell.down.level <= 1)) ) 
					{ return movedown(cell, coords, path, direction);}
				if(newdir == "left" && ((cell.left.type == 0) || (cell.left.type == 2 && cell.left.level <= 1)) ) 
					{return moveleft(cell, coords, path, direction);}
				if(newdir == "right"&& ((cell.right.type == 0) || (cell.right.type == 2 && cell.right.level <= 1)) ) 
					{return moveright(cell, coords, path, direction);}
				if(newdir == undefined){
					//couldnt find a new spot in this direction
					direction = backtrack(path, coords.slice(), tempdir);
					backtracking = true;
					if(direction == "up" && ((cell.up.type == 0) || (cell.up.type == 2 && cell.up.level <= 1)) )
						{return moveup(cell, coords, path, direction);}
					if(direction == "down" && ((cell.down.type == 0) || (cell.down.type == 2 && cell.down.level <= 1)) )
						{ return movedown(cell, coords, path, direction);}
					if(direction == "left" && ((cell.left.type == 0) || (cell.left.type == 2 && cell.left.level <= 1)) ) 
						{return moveleft(cell, coords, path, direction);}
					if(direction == "right"&& ((cell.right.type == 0) || (cell.right.type == 2 && cell.right.level <= 1)) ) 
						{return moveright(cell, coords, path, direction);}
				
				}

				//if youre stuck and somehow the badpaths and the path have spiraled down onto you, 
				//reset your paths and drop nothing and start over
				path = [];
				badpaths = [];
				return "drop";
			}
			else{
				//we found our block, now pick it up
				if(coords[0] == nextblock[0] && coords[1] == nextblock[1] && cell.type == 2 && cell.level == 1){
					holdingBlock = true;
					//reverse the path list, walk back now
					path = path.reverse();
					return "pickup";
				}
				//else walk to the block that you found in the nextblock variable
				return walktoblock(coords, cell, nextblock, path, direction);
			}
			
		}
		
	}
	else{
		/*-------TREASURE FINDING CODE----------*/

		//pick up a block initially
		if(!holdingBlock && cell.type == 2 && cell.level == 1){
			holdingBlock = true;
			return "pickup";
		}
		//first check if you have found the treasure. if so then were almost done with this phase
		if(cell.up.type == 3){
			foundTreasure = true;
			treasureHeight = cell.up.level;

			tcoords = coords.slice();
			tcoords[1] += 1;
			basestep = tcoords.slice();
			basestep[1] += 1;
			
			return moveright(cell, coords, traversed, direction);
		}
		if(cell.right.type == 3){
			foundTreasure = true;
			treasureHeight = cell.right.level;
			
			tcoords = coords.slice();
			tcoords[0] += 1;
			basestep = tcoords.slice();
			basestep[1] += 1;

			return gotobase(coords, basestep, cell, traversed, direction)
		}
		if(cell.down.type == 3){
			foundTreasure = true;
			treasureHeight = cell.down.level;
			
			tcoords = coords.slice();
			tcoords[1] -= 1;
			basestep = tcoords.slice();
			basestep[1] += 1;
			
			if(cell.type == 0){
				
				//we were above the treasure and it was empty, increment basestep
				basestepper(basestep, tcoords, basenum);
				basenum++;
				holdingBlock = false;
				return "drop";
			}
			else{
				
				//we were above the treasure and it was not empty, move to the right and make that a base
				basestepper(basestep, tcoords, basenum);
				basenum++;
				return moveright(cell, coords, traversed, direction);
			}
		}
		if(cell.left.type == 3){
			foundTreasure = true;
			treasureHeight = cell.left.level;

			tcoords = coords.slice();
			tcoords[0] -= 1;
			basestep = tcoords.slice();
			basestep[1] += 1;

			return gotobase(coords, basestep, cell, traversed, direction)
		}


		//Backtracking code- check this first to see if you're backtracking,
		//if so then keep backtracking until you can go somewhere new
		if(backtracking == true){
			var newdir = newDirection(coords.slice(), traversed, cell);
			if(newdir == "up" && (cell.up.type != 1)) 
				{backtracking = false;
				return moveup(cell, coords, traversed, direction);}
			if(newdir == "down" && (cell.down.type != 1))
				{backtracking = false;
				return movedown(cell, coords, traversed, direction);}
			if(newdir == "left" && (cell.left.type != 1)) 
				{backtracking = false;
				return moveleft(cell, coords, traversed, direction);}
			if(newdir == "right"&& (cell.right.type != 1)) 
				{backtracking = false;
				return moveright(cell, coords, traversed, direction);}
			
			if(newdir == undefined){
				//couldnt find a new spot from any of these cells, go back one more on the traversed list.
				tempdir = direction;
			 	//direction is set from the backtrack function which just moves back one from the 
			 	//current coordinates
				direction = backtrack(traversed,coords.slice(), tempdir);
				if(direction == "up" && (cell.up.type != 1)) {return moveup(cell, coords, traversed, direction);}
				if(direction == "down" && (cell.down.type != 1)) { return movedown(cell, coords, traversed, direction);}
				if(direction == "left" && (cell.left.type != 1)) {return moveleft(cell, coords, traversed, direction);}
				if(direction == "right"&& (cell.right.type != 1)) {return moveright(cell, coords, traversed, direction);}

			}
		}
		
		//if NOT backtracking, then we simply just find a new direction to walk in, and walk in that
		//direction until we cant anymore. it traces along the edges and moves along to find the treasure
		tempdir = direction;	
		var newdir = newDirection(coords.slice(), traversed, cell);
		if(newdir == "up" && (cell.up.type != 1)) {return moveup(cell, coords, traversed, direction);}
		if(newdir == "down" && (cell.down.type != 1)) { return movedown(cell, coords, traversed, direction);}
		if(newdir == "left" && (cell.left.type != 1)) {return moveleft(cell, coords, traversed, direction);}
		if(newdir == "right"&& (cell.right.type != 1)) {return moveright(cell, coords, traversed, direction);}
		if(newdir == undefined){
			//couldnt find a new spot in this direction, start backtracking
			direction = backtrack(traversed, coords.slice(), tempdir);
			backtracking = true;
			if(direction == "up" && (cell.up.type != 1)) {return moveup(cell, coords, traversed, direction);}
			if(direction == "down" && (cell.down.type != 1)) { return movedown(cell, coords, traversed, direction);}
			if(direction == "left" && (cell.left.type != 1)) {return moveleft(cell, coords, traversed, direction);}
			if(direction == "right"&& (cell.right.type != 1)) {return moveright(cell, coords, traversed, direction);}
			
			}
		}
	

}

//matches 2 sets of coordinates, returns true or false
function coordmatch(curr, orig){
	//match x and y 
	if(curr[0] == orig[0] && curr[1] == orig[1]){
		return true;
	}
	else
		return false;
}
//walks up the staircase
function walkup(coords, tcoords){
	//these 6 cells are the cells that we reserve around the treasure as 'basesteps'
	//we simply match those cells with the current coords to find out where it goes next
	cell1 = tcoords.slice(), cell2 = tcoords.slice(), cell3 = tcoords.slice(), cell4 = tcoords.slice(),
	cell5 = tcoords.slice(), cell6 = tcoords.slice();
	cell1[1] += 1, cell2[0] += 1, cell2[1] += 1, cell3[0] += 1,cell4[0] += 1,cell4[1] -= 1,
	cell5[1] -= 1, cell6[0] -= 1, cell6[1] -= 1;

	if (coordmatch(coords, cell2)){
		return "left";
	}
	if (coordmatch(coords, cell3)){
		return "up";
	}
	if (coordmatch(coords, cell4)){
		return "up";
	}
	if (coordmatch(coords, cell5)){
		return "right";
	}
	if (coordmatch(coords, cell6)){
		return "right";
	}
}
//walks down the staircase
function walkdown(coords, tcoords, cell, path, direction){
	//these 6 cells are the cells that we reserve around the treasure as 'basesteps'
	//we simply match those cells with the current coords to find out where it goes next
	cell1 = tcoords.slice(), cell2 = tcoords.slice(), cell3 = tcoords.slice(), cell4 = tcoords.slice(),
	cell5 = tcoords.slice(), cell6 = tcoords.slice();
	cell1[1] += 1, cell2[0] += 1, cell2[1] += 1, cell3[0] += 1,cell4[0] += 1,cell4[1] -= 1,
	cell5[1] -= 1, cell6[0] -= 1, cell6[1] -= 1;
	//cant walk up 1 cellwalkdown
	if (coordmatch(coords, cell1) && cell.level >= 1){
		return moveright(cell, coords, path, direction);
	}
	if (coordmatch(coords, cell2) && cell.level >= 1){
		return movedown(cell, coords, path, direction);
	}
	if (coordmatch(coords, cell3) && cell.level >= 1){
		return movedown(cell, coords, path, direction);
	}
	if (coordmatch(coords, cell4) && cell.level >= 1){
		return moveleft(cell, coords, path, direction);
	}
	if (coordmatch(coords, cell5) && cell.level >= 1){
		return moveleft(cell, coords, path, direction);
	}

}
//checks if coords are in the list so far
function alreadyTraversed(tlist, curr, direction, cell){
	//finds out if the list already contains the current coords, curr
	if(direction == "up"){
		curr[1] = curr[1] + 1;
	}
	if(direction == "down"){
		curr[1] = curr[1] - 1;
	}
	if(direction == "right"){
		curr[0] = curr[0] + 1;
	}
	if(direction == "left"){
		curr[0] = curr[0] - 1;
	}

	for(i = 0; i < tlist.length; i++){
		if(tlist[i][0] == curr[0] && tlist[i][1] == curr[1]){
			return true;
		}
	}
	return false;
}
//finds a new direction from the current cell
function newDirection(coords, traversed, cell){
	//check if youve already traversed those coordinates, and that new spot is 
	//empty or a block of 1
	if(!alreadyTraversed(traversed, coords.slice(), "up", cell) && 
		((cell.up.type == 0) || (cell.up.type == 2 && cell.up.level <= 1)) ){
		return "up";
	} 
	else if(!alreadyTraversed(traversed, coords.slice(), "right", cell) && 
		((cell.right.type == 0) || (cell.right.type == 2 && cell.right.level <= 1)) ){
		return "right";
	}
	else if(!alreadyTraversed(traversed, coords.slice(), "down", cell) && 
		((cell.down.type == 0) || (cell.down.type == 2 && cell.down.level <= 1)) ){
		return "down";
	}
	else if(!alreadyTraversed(traversed, coords.slice(), "left", cell) && 
		((cell.left.type == 0) || (cell.left.type == 2 && cell.left.level <= 1)) ){
		return "left";
	}
}
//finds a new direction, just like newDirection except it checks if the coords are on badpaths too
function newDirection2(coords, path, badpaths, cell){

	if(!alreadyTraversed(path, coords.slice(), "up", cell) && 
		!alreadyTraversed(badpaths, coords.slice(), "up", cell) &&
		((cell.up.type == 0) || (cell.up.type == 2 && cell.up.level <= 1)) ){
		return "up";
	} 
	else if(!alreadyTraversed(path, coords.slice(), "right", cell) && 
		!alreadyTraversed(badpaths, coords.slice(), "right", cell) &&
		((cell.right.type == 0) || (cell.right.type == 2 && cell.right.level <= 1)) ){
		return "right";
	}
	else if(!alreadyTraversed(path, coords.slice(), "down", cell) && 
		!alreadyTraversed(badpaths, coords.slice(), "down", cell) &&
		((cell.down.type == 0) || (cell.down.type == 2 && cell.down.level <= 1)) ){
		return "down";
	}
	else if(!alreadyTraversed(path, coords.slice(), "left", cell) && 
		!alreadyTraversed(badpaths, coords.slice(), "left", cell) &&
		((cell.left.type == 0) || (cell.left.type == 2 && cell.left.level <= 1)) ){
		return "left";
	}
}
//backtracks from current spot
function backtrack(tlist, curr, direction){
	//if the traversed list, tlist, is empty then something is wrong, so return
	if(tlist[0][0] == curr[0] && tlist[0][1] == curr[1]){
		return;
	}
	//find the first instance of the coordinates 'curr' and go to the coords before them
	for(i = 0; i < traversed.length; i++){
		if(tlist[i][0] == curr[0] && tlist[i][1] == curr[1]){
			inc = traversed.length - i + 1;
			end = traversed.length - inc ;
			break;
		}
	}
	//return the correct coords for the coords 'curr'
	if(tlist[end][0] - curr[0] == 1){
		return "right";
	}
	if(tlist[end][0] - curr[0] == -1){
		return "left";
	}
	if(tlist[end][1] - curr[1] == 1){
		return "up";
	}
	if(tlist[end][1] - curr[1] == -1){
		return "down";
	}
}
//retrace the path variable, trying to get back to the staircase base step
function retrace(path, curr, cell, direction){
	//shift the path like a stack
	if((path[0][0] - curr[0] == 0 && path[0][1] - curr[1] == 0) && path.length > 1){
		path.shift();
	}
	//go to furthest instance of the repeated variable, delete all in between

	//move to the right location accordingly
	if(path[0][0] - curr[0] == 1){
		path = path.shift();
		return moveright(cell, coords, path, direction);
	}
	if(path[0][0] - curr[0] == -1){
		path = path.shift();
		return moveleft(cell, coords, path, direction);
	}
	if(path[0][1] - curr[1] == 1){
		path = path.shift();
		return moveup(cell, coords, path, direction);
	}
	if(path[0][1] - curr[1] == -1){
		path = path.shift();
		return movedown(cell, coords, path, direction);
	}
	
}
//pushes coordinates to a list
function coordpusher (traversed, coords){
	traversed.push(coords.slice());
}
//moves left, right, up , down and pushes coords to 'traversed'
function moveleft(cell,coords, traversed, direction){
	direction = "left";
	coords[0] -= 1;
	coordpusher(traversed, coords.slice());
	return "left";
}
function moveright(cell,coords, traversed, direction){
	direction = "right";
	coords[0] += 1;
	coordpusher(traversed, coords.slice());
	return "right";
}
function moveup(cell,coords, traversed, direction){
	direction = "up";
	coords[1] += 1;
	coordpusher(traversed, coords.slice());
	return "up";
}
function movedown(cell,coords, traversed, direction){
	direction = "down";
	coords[1] -= 1;
	coordpusher(traversed, coords.slice());
	return "down";
}
//walks from staircase to a block, avoiding obstacles as it can
function walktoblock(coords, cell, blockspot, path, direction){
	//diff between the current spot and the block were going to
	xdiff = blockspot[0] - coords[0];
	ydiff = blockspot[1] - coords[1];

	if(ydiff >= 1 && (cell.up.type == 0 || (cell.up.type == 2 && cell.up.level <= 1))){
		return moveup(cell, coords, path, direction);
	}
	if(xdiff >= 1 && (cell.right.type == 0 || (cell.right.type == 2 && cell.right.level <= 1) )){
		return moveright(cell, coords, path, direction);
	}
	if(xdiff <= -1 && (cell.left.type == 0 || (cell.left.type == 2 && cell.left.level <= 1))){
		return moveleft(cell, coords, path, direction);
	}
	if(ydiff <= -1 && (cell.down.type == 0 || (cell.down.type == 2 && cell.down.level <= 1))){
		return movedown(cell, coords, path, direction);
	}
	else{
		//directions we want to go are blocked, go in a new direction thats just empty
		if(xdiff == 0 && cell.right.type == 0){
			return moveright(cell, coords, path, direction);
		}
		if(xdiff == 0 && cell.left.type == 0){
			return moveleft(cell, coords, path, direction);
		}
		if(ydiff == 0 && cell.up.type == 0){
			return moveup(cell, coords, path, direction);
		}
		if(ydiff == 0 && cell.down.type == 0){
			return movedown(cell, coords, path, direction);
		}
		if(cell.right.type == 0){
			return moveright(cell, coords, path, direction);
		}
		if(cell.left.type == 0){
			return moveleft(cell, coords, path, direction);
		}
		if(cell.up.type == 0){
			return moveup(cell, coords, path, direction);
		}
		if(cell.down.type == 0){
			return movedown(cell, coords, path, direction);
		}
		else{
			//if the way to get there is hard, skip that block
			return;
		}
	}

}

function gotobase(coords, basestep, cell, traversed, direction){
	//strictly for getting to base from start position, same code as walktoblock()
	xdiff = basestep[0] - coords[0];
	ydiff = basestep[1] - coords[1];
	if(xdiff == 0 && ydiff == 0){
		return;
	}
	if(ydiff >= 1 && (cell.up.type == 0 ||(cell.up.type == 2 && cell.up.level <= 1)) ){
		return moveup(cell, coords, path, direction);
	}
	if(xdiff >= 1 && (cell.right.type == 0 || (cell.right.type == 2 && cell.right.level <= 1))){
		return moveright(cell, coords, path, direction);
	}
	if(xdiff <= -1 && (cell.left.type == 0 || (cell.left.type == 2 && cell.left.level <= 1))){
		return moveleft(cell, coords, path, direction);
	}
	if(ydiff <= 1 && (cell.down.type == 0 ||  (cell.down.type == 2 && cell.down.level <= 1))){
		return movedown(cell, coords, path, direction);
	}
	else{
		//directions we want to go are blocked, go in a new direction thats just empty
		if(xdiff == 0 && cell.right.type == 0){
			return moveright(cell, coords, path, direction);
		}
		if(xdiff == 0 && cell.left.type == 0){
			return moveleft(cell, coords, path, direction);
		}
		if(ydiff == 0 && cell.up.type == 0){
			return moveup(cell, coords, path, direction);
		}
		if(ydiff == 0 && cell.down.type == 0){
			return movedown(cell, coords, path, direction);
		}
		if(cell.right.type == 0){
			return moveright(cell, coords, path, direction);
		}
		if(cell.left.type == 0){
			return moveleft(cell, coords, path, direction);
		}
		if(cell.up.type == 0){
			return moveup(cell, coords, path, direction);
		}
		if(cell.down.type == 0){
			return movedown(cell, coords, path, direction);
		}
		else{
			console.log('dont know how to get back to base');
		}
	}

}
//finds closest block, ignoring blocks from the staircase that were mistakenly added to blocks set
function findclosest(coords, blocks, tcoords){
	//these 6 cells are the cells surrounding the treasure, aka the base steps,
	//ignore them when looking for blocks
	cell1 = tcoords.slice(), cell2 = tcoords.slice(), cell3 = tcoords.slice(), cell4 = tcoords.slice(),
	cell5 = tcoords.slice(), cell6 = tcoords.slice();
	cell1[1] += 1, cell2[0] += 1, cell2[1] += 1, cell3[0] += 1,cell4[0] += 1,cell4[1] -= 1,
	cell5[1] -= 1, cell6[0] -= 1, cell6[1] -= 1;
		diff = 100;
		temp = 0;
		returnable = [100,100];
		for(i = 0; i < blocks.length; i++){
			if( (blocks[i][0] == cell1[0] && blocks[i][1] == cell1[1]) || 
				(blocks[i][0] == cell2[0] && blocks[i][1] == cell2[1]) || 
				(blocks[i][0] == cell3[0] && blocks[i][1] == cell3[1]) || 
				(blocks[i][0] == cell4[0] && blocks[i][1] == cell4[1]) || 
				(blocks[i][0] == cell5[0] && blocks[i][1] == cell5[1]) || 
				(blocks[i][0] == cell6[0] && blocks[i][1] == cell6[1]) ) 
				{
				//skip ones that are already in position, theyll be fine
				continue;
			}
			//find the closest block in a for loop checking against all blocks
			temp = Math.abs(blocks[i][0] - coords[0]) + Math.abs(blocks[i][1] - coords[1]);
			if(temp < diff){
				diff = temp;
				index = i;
				returnable = blocks[i].slice();
			}
		}
		//remove the block from that index
		blocks.splice(index, 1);
		//return that block
		return returnable;
}
//currently at the base, same functionality as coordmatch, just diff function name so that its clear whats
//going on 
function atbase(coords, base){
	if(coords[0] == base[0] && coords[1] == base[1])
		return true;
	else 
		return false;
}
//checks if there are any blocks on this cell, and if the block is already stored
function checkblock(cell, coords, basestep, blocks){
	//checks if that cell is a block, if its not on the list it adds that block
	if(cell.type == 2 && cell.level == 1){
		temp = coords.slice();
		alreadyplaced = false;
		for(i = 0; i < blocks.length; i++){
			if(blocks[i][0] == temp[0] && blocks[i][1] == temp[1]){
				alreadyplaced = true;
			}
		}
		if(alreadyplaced != true){
			blocks.push(temp.slice());
			return 1;
		}	
	}
	return 0;
}
//figures out the new base step coordinates
function basestepper(basestep, tcoords, num){
	//checks against a number 1 2 3 4 5 to see which basestep we are on around the 
	//treasure, simple additions to the x and y to find new coords
	if(num == 1){
		basestep[0] = tcoords[0] + 1;
		basestep[1] = tcoords[1] + 1;
	}
	if(num == 2){
		basestep[0] = tcoords[0] + 1;
		basestep[1] = tcoords[1];
	}
	if(num == 3){
		basestep[0] = tcoords[0] + 1;
		basestep[1] = tcoords[1] - 1;
	}
	if(num == 4){
		basestep[0] = tcoords[0];
		basestep[1] = tcoords[1] - 1;
	}
	if(num == 5){
		basestep[0] = tcoords[0] - 1;
		basestep[1] = tcoords[1] - 1;
	}

}


}